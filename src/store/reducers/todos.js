const INITIAL_STATE = [];

export default function reducerTodo(state = INITIAL_STATE, action) {
  switch (action.type) {
    case 'ADD_TODO':
      return [
        ...state,
        {
          id: Math.random(),
          text: action.payload.text,
        },
      ];
      default:
      return state;
  }
}