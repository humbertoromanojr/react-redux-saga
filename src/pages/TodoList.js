import React from 'react';

import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import * as actions from '../store/actions'

const TodoList = ({ todos, addTodo }) => (
  <div>
    <ul>
    {todos.map(todo => (
        <li key={todo.id}>{todo.text}</li>
      ))}
    </ul>
    <button onClick={() => addTodo('Fazer Café')}>Novo Todo</button>
  </div>
)

const mapStateToProps = state => ({
  todos: state.todos
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(actions, dispatch);

export default connect(
  mapStateToProps, 
  mapDispatchToProps
  )(TodoList);
